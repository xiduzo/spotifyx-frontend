FROM node:14.1-alpine AS builder

WORKDIR /opt/web

# ADD files
COPY tsconfig.json .
COPY .env .
COPY public public
COPY package.json package-lock.json ./

# Install dependencies
RUN npm ci --silent

# Build the image
COPY src src
RUN npm run build
ENV PATH="./node_modules/.bin:$PATH"

# Cleanup
RUN rm -rf src tsconfig.json package.json package-lock.json

# Prepare server
FROM nginx:1.17-alpine
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
  chmod +x envsubst && \
  mv envsubst /usr/local/bin

# Add hosting files
COPY ./nginx.config /etc/nginx/nginx.template
COPY --from=builder /opt/web/build /usr/share/nginx/html

# Run the server
CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
