import {
  CssBaseline,
  darken,
  SimplePaletteColorOptions,
  StyledEngineProvider,
  Theme,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material'
import { grey } from '@mui/material/colors'
import { alpha, createTheme } from '@mui/material/styles'
import { FC, useState } from 'react'

const greeny: SimplePaletteColorOptions = {
  dark: '#021600',
  main: '#5FFF95',
  light: '#CBFFE3',
  contrastText: '#CBFFE3',
}

const pinky: SimplePaletteColorOptions = {
  dark: '#26001F',
  main: '#FF5FE5',
  light: '#FFCAF7',
  contrastText: '#FFCAF7',
}

const orangey: SimplePaletteColorOptions = {
  dark: '#1C0A00',
  main: '#FF5F5F',
  light: '#FFC9C9',
  contrastText: '#FFC9C9',
}

const bluey: SimplePaletteColorOptions = {
  dark: '#001428',
  main: '#5FB2FF',
  light: '#CBF9FF',
  contrastText: '#CBF9FF',
}

const sunny: SimplePaletteColorOptions = {
  dark: '#241800',
  main: '#FFBF5F',
  light: '#FFC9C9',
  contrastText: '#FFC9C9',
}

const limey: SimplePaletteColorOptions = {
  dark: '#0B1A00',
  main: '#CCFF5F',
  light: '#FFFED9',
  contrastText: '#FFFED9',
}

const possibleThemes = [greeny, pinky, orangey, bluey, sunny, limey]

export const getThemeGradient = (mainColor: string): string => {
  switch (mainColor) {
    case greeny.main:
      return 'linear-gradient(135deg, #5FFF95 0, #5FFFEC 100%)'
    case pinky.main:
      return 'linear-gradient(135deg, #FF5FE5 0, #FF5F72 100%)'
    case orangey.main:
      return 'linear-gradient(135deg, #FF5F5F 0, #FF995F 100%)'
    case bluey.main:
      return 'linear-gradient(135deg, #5FB2FF 0, #18FFF1 100%)'
    case sunny.main:
      return 'linear-gradient(135deg, #FFBF5F 0, #DFFF5F 100%)'
    case limey.main:
      return 'linear-gradient(135deg, #FFF95F 0, #BCFF4E 100%)'
    default:
      return 'linear-gradient(135deg, #FF5FE5 0, #FF5F72 100%)'
  }
}

const colorTheme = () => {
  const themeOption = Math.floor(Math.random() * possibleThemes.length)

  const local = localStorage.getItem('themeOption')
  if (local === null) localStorage.setItem('themeOption', JSON.stringify(themeOption))

  return createTheme({
    palette: {
      primary: possibleThemes[themeOption],
      // primary:
      //   local !== null ? possibleThemes[Number(local)] : possibleThemes[themeOption],
      secondary: grey,
      mode: 'dark',
    },
    shape: {
      borderRadius: 12,
    },
  })
}

const getTheme = (baseTheme: Theme): Theme => {
  const borderWidth = 3

  return createTheme({
    palette: baseTheme.palette,
    typography: {
      fontFamily: 'Inter, monospace, Roboto, Helvetica, Arial, sans-serif',
    },
    components: {
      MuiTypography: {
        styleOverrides: {
          root: {
            color: baseTheme.palette.primary.light,
          },
          h1: {
            fontWeight: 700,
            fontSize: '2rem',
            lineHeight: '120%',
          },
          h2: {
            fontWeight: 700,
            fontSize: '1.5rem',
            lineHeight: '120%',
          },
          h3: {
            fontWeight: 700,
            fontSize: '1.25rem',
            lineHeight: '120%',
          },
          h4: {
            fontWeight: 600,
            fontSize: '1.125rem',
            lineHeight: '120%',
          },
          h5: {
            fontWeight: 500,
            fontSize: '1.5rem',
            lineHeight: '135%',
          },
          h6: {
            fontWeight: 500,
            fontSize: '1.125rem',
            lineHeight: '146%',
          },
          body1: {
            fontWeight: 400,
            fontSize: '1rem',
            lineHeight: '155%',
          },
          body2: {
            fontWeight: 400,
            fontSize: '0.875rem',
            lineHeight: '120%',
          },
        },
      },
      MuiListItem: {
        styleOverrides: {
          root: {
            paddingLeft: 0,
            paddingRight: 0,
          },
        },
      },
      MuiListItemText: {
        styleOverrides: {
          primary: {
            fontWeight: 600,
            fontSize: '1.125rem',
            lineHeight: '120%',
          },
          secondary: {
            fontWeight: 400,
            fontSize: '0.875rem',
            lineHeight: '120%',
            opacity: 0.6,
            marginTop: baseTheme.spacing(1),
          },
        },
      },
      MuiAvatar: {
        styleOverrides: {
          rounded: {
            borderRadius: baseTheme.shape.borderRadius,
          },
        },
      },
      MuiFab: {
        styleOverrides: {
          root: {
            background: getThemeGradient(baseTheme.palette.primary.main),
            color: baseTheme.palette.primary.dark,
            borderRadius: baseTheme.shape.borderRadius,
            position: 'fixed',
            bottom: baseTheme.spacing(3),
            right: baseTheme.spacing(3),
            zIndex: 1001,
            '& > svg': {
              width: '1.5em',
              height: '1.5em',
            },
          },
        },
      },
      MuiIconButton: {
        styleOverrides: {
          colorPrimary: {
            background: alpha(baseTheme.palette.primary.dark, 0.2),
            color: baseTheme.palette.primary.dark,
          },
        },
      },
      MuiToggleButton: {
        styleOverrides: {
          root: {
            border: 0,
            '&.Mui-disabled': {
              border: 0,
            },
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          root: {
            fontWeight: 700,
            fontSize: '1.25rem',
            lineHeight: '120%',
            textTransform: 'none',
            borderRadius: baseTheme.shape.borderRadius,
          },
          sizeMedium: {
            padding: '24px 44px',
          },
          containedSizeMedium: {
            padding: '24px 44px',
          },
          containedPrimary: {
            background: baseTheme.palette.primary.main,
            color: baseTheme.palette.getContrastText(baseTheme.palette.primary.main),
            '&:hover': {
              background: darken(baseTheme.palette.primary.main, 0.25),
            },
          },
        },
      },
      MuiDialog: {
        styleOverrides: {
          root: {
            background: darken(baseTheme.palette.primary.dark, 0.25),
          },
          paper: {
            background: darken(baseTheme.palette.primary.dark, 0.25),
          },
        },
      },
      MuiPaper: {
        styleOverrides: {
          root: {
            background: darken(baseTheme.palette.primary.dark, 0.25),
          },
        },
      },
      MuiInput: {
        styleOverrides: {
          underline: {
            '&::after, &::before': {
              borderRadius: baseTheme.shape.borderRadius,
              borderBottomWidth: `${borderWidth}px !important`,
            },
            '&::before': {
              height: borderWidth,
            },
          },
        },
      },
      MuiDivider: {
        styleOverrides: {
          root: {
            borderBottomWidth: 'medium',
            borderColor: baseTheme.palette.primary.dark,
            opacity: 0.1,
          },
        },
      },
      MuiToolbar: {
        styleOverrides: {
          root: {
            background: baseTheme.palette.primary.dark,
          },
        },
      },
      MuiOutlinedInput: {
        styleOverrides: {
          root: {
            borderRadius: 0,
            '& fieldset': {
              border: 'none',
              borderBottom: `${borderWidth}px solid ${baseTheme.palette.primary.light}`,
            },
          },
        },
      },
      MuiSlider: {
        styleOverrides: {
          rail: {
            height: 4,
            color: baseTheme.palette.primary.light,
            opacity: 1,
          },
          track: {
            height: 4,
          },
          thumb: {
            display: 'none',
          },
        },
      },
      MuiTextField: {
        styleOverrides: {
          root: {
            borderRadius: 0,
          },
        },
      },
    },
  })
}

export const ThemeProvider: FC = props => {
  /**
   * Component state
   */
  const { children } = props

  const [theme] = useState<Theme>(getTheme(colorTheme()))

  /**
   * Methods
   */
  return (
    <StyledEngineProvider injectFirst>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </MuiThemeProvider>
    </StyledEngineProvider>
  )
}
