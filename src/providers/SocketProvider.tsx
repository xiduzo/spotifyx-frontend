import { createContext, FC, useEffect, useState } from 'react'
import { io, Socket } from 'socket.io-client'

export const SocketContext = createContext<{ socket: null | Socket }>({ socket: null })

export const SocketProvider: FC = props => {
  /*
   * Component state
   */
  const { children } = props

  const socketHost = window.location.hostname.includes('localhost')
    ? 'localhost:8000'
    : 'https://spotify-x-sockets.herokuapp.com'

  const [socket, setSocket] = useState<Socket | null>(null)

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */
  useEffect(() => {
    const newSocket = io(socketHost, {})
    newSocket.connect()
    newSocket.on('connect', () => {
      setSocket(newSocket)
    })

    newSocket.on('disconnect', () => {
      setSocket(null)
    })

    return () => {
      newSocket.disconnect()
      setSocket(null)
    }
  }, [])

  /*
   * Return
   */

  return <SocketContext.Provider value={{ socket }}>{children}</SocketContext.Provider>
}
