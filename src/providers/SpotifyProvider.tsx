import { createContext, FC, useCallback, useEffect, useRef, useState } from 'react'
import SpotifyWebApi from 'spotify-web-api-js'
import { useSocket } from '../hooks/useSocket'
import {
  SpotifyAuthResponse,
  SpotifyRefreshResponse,
  SPOTIFY_AUTH_ENDPOINT,
} from '../lib/constants/spotifyConstants'

interface LocalSpotifyAuth {
  storedAt: Date
  expiresAt: Date
  accessToken: string
  refreshToken: string
}

interface Context {
  spotifyAuth: () => Promise<string>
  spotify: SpotifyWebApi.SpotifyWebApiJs
  userId?: string
}

export const SpotifyContext = createContext<Context>({
  spotifyAuth: (): Promise<string> => {
    throw new Error('not implemented')
  },
  spotify: new SpotifyWebApi(),
})

export const SpotifyProvider: FC = props => {
  /*
   * Component state
   */
  const { children } = props

  const [userId, setUserId] = useState<string>()

  const authCode = useRef<string>()
  const popup = useRef<Window | null>()

  const spotify = new SpotifyWebApi()

  const windowToken = window.location.search.replace('?code=', '')

  /*
   * Custom & 3th party hooks
   */
  const { socket } = useSocket()

  /*
   * Methods
   */
  // @ts-ignore
  window.spotifyCallback = callbackCode => {
    popup.current?.close()
    authCode.current = callbackCode
  }

  const addHoursToDate = (date: Date, hours: number): Date =>
    new Date(new Date(date).setHours(date.getHours() + hours))

  const updateLocalStorage = (
    response: SpotifyAuthResponse | SpotifyRefreshResponse
  ): number => {
    const now = new Date()
    const current = JSON.parse(localStorage.getItem('spotify_token') || '{}')
    const overwrite = {
      accessToken: response.body.access_token,
      refreshToken:
        (response as SpotifyAuthResponse).body.refresh_token ??
        current.refreshToken ??
        undefined,
      expiresAt: addHoursToDate(now, response.body.expires_in / 60 / 60),
      storedAt: now,
    }

    localStorage.setItem(
      'spotify_token',
      JSON.stringify({
        ...current,
        ...overwrite,
      })
    )

    return response.body.expires_in * 1000
  }

  const spotifyAuth = (): Promise<string> => {
    authCode.current = ''
    popup.current = window.open(
      SPOTIFY_AUTH_ENDPOINT,
      'Login with Spotify',
      'width=800,height=600'
    )

    return new Promise(resolve => {
      const waitForToken = () => {
        if (authCode.current) return resolve(authCode.current)
        requestAnimationFrame(waitForToken)
      }

      requestAnimationFrame(waitForToken)
    })
  }

  const getLocalTokens = useCallback((): LocalSpotifyAuth | undefined => {
    const localToken = localStorage.getItem('spotify_token')
    if (!localToken) return

    const parsedToken = JSON.parse(localToken) as LocalSpotifyAuth
    return parsedToken
  }, [])

  const tokenValidationTime = useCallback((): Promise<number> => {
    const tokens = getLocalTokens()
    if (!tokens) return Promise.resolve(0)

    const isValidFor = new Date(tokens?.expiresAt).getTime() - new Date().getTime()
    if (isValidFor <= 0) return Promise.resolve(0)

    spotify.setAccessToken(tokens.accessToken)
    return Promise.resolve(isValidFor)
  }, [getLocalTokens])

  const authenticate = useCallback(async (): Promise<string | number> => {
    const validFor = await tokenValidationTime()
    if (validFor > 0) return Promise.resolve(validFor)

    const code = await spotifyAuth()
    return await Promise.resolve(code)
  }, [tokenValidationTime])

  const getRefreshToken = useCallback(async () => {
    const token = getLocalTokens()?.refreshToken
    if (!token) return Promise.reject('No spotify refresh token available')

    return Promise.resolve(token)
  }, [getLocalTokens])

  const getAccessToken = useCallback(async () => {
    const token = getLocalTokens()?.accessToken
    if (!token) return Promise.reject('No spotify access token available')

    return Promise.resolve(token)
  }, [getLocalTokens])

  const autoRefreshToken = useCallback(
    (timeout: number) => {
      const getToken = () => {
        getRefreshToken()
          .then(token => {
            socket?.emit('auth:refresh:spotify', token)
          })
          .catch(Promise.reject)
      }

      setTimeout(getToken, Math.max(100, timeout - 1000 * 60))
    },
    [getRefreshToken, socket]
  )

  const setUser = useCallback(async (): Promise<void> => {
    try {
      const response = await spotify.getMe()
      setUserId(response.id)
    } catch (error: any) {
      return Promise.reject(error)
    }
  }, [setUserId])

  /*
   * Side effects
   */
  useEffect(() => {
    if (!windowToken) return

    // @ts-ignore
    window.opener.spotifyCallback(windowToken)
  }, [windowToken])

  // Authenticate
  useEffect(() => {
    if (authCode.current) return
    if (!socket) return

    authenticate().then(response => {
      const validFor = parseInt(`${response}`, 10)
      if (isNaN(validFor)) {
        if (!authCode.current) return
        socket.emit('auth:request:spotify', authCode.current)
        return
      }

      // TODO: can we somehow not save / send the access token but work with the refresh token instead?
      getAccessToken().then(token => {
        socket.emit('auth:token:spotify', token)
        autoRefreshToken(validFor)
        setUser()
      })
    })

    const setToken = (response: SpotifyAuthResponse | SpotifyRefreshResponse) => {
      const validFor = updateLocalStorage(response)
      spotify.setAccessToken(response.body.access_token)
      autoRefreshToken(validFor)
      setUser()
    }

    socket.on('auth:response:spotify', setToken)
    socket.on('auth:refreshed:spotify', setToken)

    return () => {
      socket.off('auth:response:spotify')
      socket.off('auth:refreshed:spotify')
    }
  }, [socket, spotify, autoRefreshToken, setUser, authenticate])

  /*
   * Return
   */

  return (
    <SpotifyContext.Provider value={{ spotify, userId, spotifyAuth }}>
      {children}
    </SpotifyContext.Provider>
  )
}
