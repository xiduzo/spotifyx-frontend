import React from 'react'
import { Frame } from './modules/Frame'
import { SocketProvider } from './providers/SocketProvider'
import { SpotifyProvider } from './providers/SpotifyProvider'
import { ThemeProvider } from './providers/ThemeProvider'
import { NotificationsProvider } from 'reapop'
import { Provider as StateProvider } from 'react-redux'
import { store } from './redux/store'

const App = () => (
  <SocketProvider>
    <SpotifyProvider>
      <StateProvider store={store}>
        <NotificationsProvider>
          <ThemeProvider>
            <Frame />
          </ThemeProvider>
        </NotificationsProvider>
      </StateProvider>
    </SpotifyProvider>
  </SocketProvider>
)

export default App
