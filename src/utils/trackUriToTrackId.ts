export const trackUriToTrackId = (uri: string): string =>
  uri.replace('spotify:track:', '')
