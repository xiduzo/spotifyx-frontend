import { Sign, Vote } from '../redux/reducer'

export const countVotes = (votes: Vote[], sign: Sign): number =>
  votes.filter(_vote => _vote.sign === sign).length
