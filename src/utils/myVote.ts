import { Sign, Vote } from '../redux/reducer'

export const myVote = (votes: Vote[], userId?: string): Sign | undefined => {
  if (!userId) return

  return votes.find(_vote => _vote.by === userId)?.sign
}
