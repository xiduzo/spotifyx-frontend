export const getThemeIndicator = (): string => {
  const local = localStorage.getItem('themeOption')

  switch (Number(local)) {
    case 0:
      return '🟢'
    case 1:
      return '🟣'
    case 2:
      return '🔴'
    case 3:
      return '🔵'
    case 4:
      return '🟠'
    case 5:
      return '🟡'
    default:
      return '⚫'
  }
}
