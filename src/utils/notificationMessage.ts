export const notificationMessage = (message: string, emoji?: string): string => {
  if (!emoji) return message

  return `<div style="display: flex; align-items: center;">
        <span style="font-size: 32px; margin-right: 16px">${emoji}</span>
        <span>${message}</span>
    <div>`
}
