const createTextArea = (): HTMLTextAreaElement => {
  const textArea = document.createElement('textarea')

  // Avoid scrolling to bottom
  textArea.style.top = '0'
  textArea.style.left = '0'
  textArea.style.position = 'fixed'

  document.body.appendChild(textArea)

  return textArea
}

const fallbackWriteTextToClipboardAsync = async (text: string): Promise<boolean> => {
  const textArea = createTextArea()

  try {
    textArea.value = text
    textArea.focus()
    textArea.select()

    const successful = document.execCommand('copy')

    if (successful) return Promise.resolve(true)

    return Promise.reject()
  } catch {
    return Promise.reject()
  } finally {
    document.body.removeChild(textArea)
  }
}

export const writeTextToClipboardAsync = async (text: string): Promise<boolean> => {
  if (!navigator.clipboard) {
    return fallbackWriteTextToClipboardAsync(text)
  }

  try {
    await navigator.clipboard.writeText(text)
    return Promise.resolve(true)
  } catch {
    return Promise.reject(false)
  }
}

export const readTextFromClipboardAsync = async (): Promise<string> => {
  if (!navigator.clipboard) {
    return Promise.reject() // TODO add fallback
  }

  try {
    const text = await navigator.clipboard.readText()
    return Promise.resolve(text)
  } catch {
    return Promise.reject()
  }
}
