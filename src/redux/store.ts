import { applyMiddleware, compose, createStore } from 'redux'
import ReduxThunk from 'redux-thunk'
import { rootReducer } from './reducer'

const withDevTools =
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ trace: false })

const composeEnhancers = withDevTools ?? compose

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(ReduxThunk))
)
