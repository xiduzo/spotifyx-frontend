import { Reducer } from 'redux'
import { RoomAction } from './actions'

export type UserId = string
export type TrackId = string
export enum Sign {
  Positive,
  Negative,
}

export interface Vote {
  by: UserId
  track: TrackId
  at: Date
  sign: Sign
}

export interface RootState {
  pin: string
  id: string
  votes: Vote[]
  progress_ms: number
  queue: string[]
  /**
   * A local database of tracks which are already fetched from the Spotify API
   * stored as `{ [trackUri]: SpotifyApi.TrackObjectFull }`
   */
  localTracks: SpotifyApi.TrackObjectFull[]
}

const initialState: RootState = {
  pin: '',
  id: '',
  queue: [],
  votes: [],
  progress_ms: 0,
  localTracks: [...JSON.parse(localStorage.getItem('localTracks') ?? '[]')],
}

export const rootReducer: Reducer<RootState, RoomAction> = (...reducer) => {
  const [state = initialState, action] = reducer

  switch (action.type) {
    case 'update-room':
      return {
        ...state,
        ...action.room,
      }
    case 'set-queue':
      return {
        ...state,
        queue: action.trackUris,
      }
    case 'set-votes':
      return {
        ...state,
        votes: action.votes,
      }
    case 'set-progress':
      return {
        ...state,
        progress_ms: action.progress_ms ?? state.progress_ms,
      }
    case 'add-to-local-db':
      if (action.tracks.length === 0) return state
      localStorage.setItem(
        'localTracks',
        JSON.stringify(Array.from(new Set([...state.localTracks, ...action.tracks])))
      )
      return {
        ...state,
        localTracks: Array.from(new Set([...state.localTracks, ...action.tracks])),
      }
    default:
      return state
  }
}
