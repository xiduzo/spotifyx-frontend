import { RootState, Vote } from './reducer'

/**
 * Room actions
 */
type UpdateRoom = {
  type: 'update-room'
  room: RootState
}

type SetVotes = {
  type: 'set-votes'
  votes: Vote[]
}

type SetQueue = {
  type: 'set-queue'
  trackUris: string[]
}

type AddTracksToLocalDb = {
  type: 'add-to-local-db'
  tracks: SpotifyApi.TrackObjectFull[]
}

type SetProgress = {
  type: 'set-progress'
  progress_ms?: number
}

export const addTracksToLocalDb = (
  tracks: SpotifyApi.TrackObjectFull[]
): AddTracksToLocalDb => ({
  type: 'add-to-local-db',
  tracks,
})

export const updateRoom = (room: RootState): UpdateRoom => ({
  type: 'update-room',
  room,
})

export const setVotes = (votes: Vote[]): SetVotes => ({
  type: 'set-votes',
  votes,
})

export const setQueue = (trackUris: string[]): SetQueue => ({
  type: 'set-queue',
  trackUris,
})

export const setProgress = (progress_ms?: number): SetProgress => ({
  type: 'set-progress',
  progress_ms,
})

export type RoomAction =
  | UpdateRoom
  | SetVotes
  | SetQueue
  | AddTracksToLocalDb
  | SetProgress
