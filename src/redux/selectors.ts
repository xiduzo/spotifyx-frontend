import { createSelector } from 'reselect'
import { RootState } from './reducer'

export const selectVotes = (state: RootState) => state.votes
export const selectTrackVotes = (trackId?: string) =>
  createSelector(selectVotes, _votes => _votes.filter(vote => vote.track === trackId))

export const selectRoom = (state: RootState) => state
export const selectRoomPin = (state: RootState) => state.pin
export const selectQueue = (state: RootState) => state.queue
export const selectLocalTracks = (state: RootState) => state.localTracks
export const selectProgress = (state: RootState) => state.progress_ms

export const selectActiveTrack = createSelector(selectQueue, _queue =>
  _queue.length > 0 ? _queue[0] : undefined
)

// Local spotify handling
export const selectTrack = (trackUri?: string) =>
  createSelector(selectLocalTracks, _localTracks =>
    trackUri ? _localTracks.find(_track => _track.uri === trackUri) : undefined
  )

export const selectMissingTracks = createSelector(
  selectLocalTracks,
  selectQueue,
  (_localTracks, _queue) => {
    const localTrackUris = _localTracks.map(_track => _track.uri)

    return _queue?.filter(trackUri => !localTrackUris.includes(trackUri)) ?? []
  }
)
