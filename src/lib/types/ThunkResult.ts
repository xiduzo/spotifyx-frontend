import { ThunkAction } from 'redux-thunk'
import { RootState } from '../../redux/reducer'

type Action = any

export type ThunkResult = ThunkAction<void | Promise<void>, RootState, void, Action>
