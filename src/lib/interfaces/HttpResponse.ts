import { HttpStatusCode } from './HttpStatusCode'

export interface HttpResponse<Body> {
  body: Body
  statusCode: HttpStatusCode
  headers: object
}
