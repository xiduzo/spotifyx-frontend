import { HttpResponse } from '../interfaces/HttpResponse'

const SPOTIFY_CLIENT_ID = 'a2a88c4618324942859ce3e1f888b938'

const SPOTIFY_REDIRECT_URL = window.location.host.includes('3000')
  ? `http://${window.location.host}`
  : `https://${window.location.host}`

const SPOTIFY_REQUIRED_SCOPES = [
  'user-modify-playback-state',
  'user-read-playback-state',
  'user-read-recently-played',
  'user-read-currently-playing',
  'playlist-modify-public',
  'playlist-read-private',
  'playlist-read-collaborative',
  'playlist-modify-public',
  'playlist-modify-private',
]

export const SPOTIFY_AUTH_ENDPOINT = `https://accounts.spotify.com/authorize?client_id=${SPOTIFY_CLIENT_ID}&redirect_uri=${SPOTIFY_REDIRECT_URL}&response_type=code&scope=${encodeURIComponent(
  SPOTIFY_REQUIRED_SCOPES.join(' ')
)}`

interface BaseAuthResponse {
  access_token: string
  /** In seconds */
  expires_in: number
  token_type: string
}

interface AuthResponse extends BaseAuthResponse {
  refresh_token: string
}

interface RefreshResponse extends BaseAuthResponse {
  scope: string
}

export type SpotifyAuthResponse = HttpResponse<AuthResponse>
export type SpotifyRefreshResponse = HttpResponse<RefreshResponse>
