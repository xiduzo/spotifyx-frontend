import { MutableRefObject, useEffect } from 'react'

export const useClickOutside = (
  ref: MutableRefObject<HTMLElement | undefined>,
  handler: () => void,
  enabled = true
) => {
  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (!ref.current) return

      const htmlTarget = event.target as HTMLElement
      const refId = ref.current.id
      let targetId = htmlTarget.id
      let check: HTMLElement | null = htmlTarget
      while (targetId !== refId && check !== null) {
        targetId = check.id
        check = check.parentElement
      }
      if (refId === targetId) return

      if (enabled) handler()
    }

    document.addEventListener('mousedown', handleClickOutside)

    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [ref, enabled])
}
