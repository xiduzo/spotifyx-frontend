import { useEffect, useRef } from 'react'

type DebounceCallback<Value> = (value: Value, previousValue?: Value) => void

export const useDebounce = <Value>(
  value: Value,
  msDelay: number,
  onDebounce: DebounceCallback<Value>
) => {
  const initialValue = useRef(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      if (initialValue.current === value) return

      if (onDebounce) onDebounce(value, initialValue.current)
      initialValue.current = value
    }, msDelay)

    return () => {
      clearTimeout(handler)
    }
  }, [value, msDelay, onDebounce])
}
