import { useCallback, useContext } from 'react'
import { HttpStatusCode } from '../lib/interfaces/HttpStatusCode'
import { SpotifyContext } from '../providers/SpotifyProvider'
import { trackUriToTrackId } from '../utils/trackUriToTrackId'

export const useSpotify = () => {
  /**
   * Component state
   */
  const { spotify, userId, spotifyAuth } = useContext(SpotifyContext)

  /**
   * Custom & 3th party hooks
   */

  /**
   * Methods
   */
  const search = useCallback(
    async (
      query: string,
      tries = 3
    ): Promise<SpotifyApi.SearchResponse | undefined> => {
      try {
        const response = await spotify.search(
          query,
          ['track', 'album', 'artist', 'playlist'],
          { limit: 30 }
        )
        return response
      } catch (error: any) {
        if (error.status === HttpStatusCode.UNAUTHORIZED) {
          if (tries <= 0) return Promise.reject(error)
          return new Promise(resolve => {
            setTimeout(async () => {
              const response = await search(query, tries--)
              resolve(response)
            }, 1000)
          })
        }
      }
    },
    [spotify]
  )

  const getTracks = useCallback(
    async (trackUris: string[]): Promise<SpotifyApi.TrackObjectFull[]> => {
      // We can only request 50 tracks at a time due to spotify constraints
      const spotifyMaxTracksPerRequest = 50

      const tracksToFetch = trackUris.map(trackUriToTrackId)
      let received: SpotifyApi.TrackObjectFull[] = []
      let canContinue = true

      do {
        try {
          const response = await spotify.getTracks(
            tracksToFetch.slice(
              received.length,
              received.length + spotifyMaxTracksPerRequest
            )
          )
          received = received.concat(response.tracks)
        } catch (error) {
          canContinue = false
        }
      } while (received.length < trackUris.length && canContinue)

      return received
    },
    [spotify]
  )

  const getPlaylistTracks = useCallback(
    async (playlistId: string): Promise<SpotifyApi.TrackObjectFull[]> => {
      let total = 0
      let canContinue = true
      let received: SpotifyApi.TrackObjectFull[] = []

      do {
        try {
          const response = await spotify.getPlaylistTracks(playlistId, {
            offset: received.length,
          })

          total = response.total

          const tracks = response.items
            .map(item => item.track)
            .filter(track => track.type === 'track') as SpotifyApi.TrackObjectFull[]

          received = received.concat(tracks)
        } catch {
          canContinue = false
        }
      } while (received.length < total && canContinue)

      return received
    },
    [spotify]
  )

  // TODO: cache this
  const getUserPlayLists = useCallback(async (): Promise<
    SpotifyApi.PlaylistObjectSimplified[]
  > => {
    let total = 0
    let canContinue = true
    let received: SpotifyApi.PlaylistObjectSimplified[] = []

    do {
      try {
        const response = await spotify.getUserPlaylists(undefined, {
          offset: received.length,
        })

        total = response.total

        received = received.concat(response.items)
      } catch {
        canContinue = false
      }
    } while (received.length < total && canContinue)

    return received
  }, [spotify])

  /**
   * Side effects
   */

  /**
   * Return
   */
  return {
    search,
    userId,
    getPlaylistTracks,
    getUserPlayLists,
    getTracks,
    spotifyAuth,
  }
}
