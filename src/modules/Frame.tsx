import { Box } from '@mui/material'
import { FC, Suspense, useEffect } from 'react'
import { BrowserRouter } from 'react-router-dom'
import NotificationsSystem, {
  setUpNotifications,
  useNotifications,
  wyboTheme,
} from 'reapop'
import { Routes } from './Routes'

export const Frame: FC = () => {
  /*
   * Component state
   */

  /*
   * Custom & 3th party hooks
   */
  const { notifications, dismissNotification } = useNotifications()

  /*
   * Methods
   */

  /*
   * Side effects
   */
  useEffect(() => {
    setUpNotifications({
      defaultProps: {
        position: 'top-center',
        dismissible: true,
        dismissAfter: 5_000,
        allowHTML: true,
      },
    })
  }, [])

  /*
   * Return
   */
  return (
    <Box
      component='main'
      sx={{
        backgroundColor: 'primary.dark',
        flexGrow: 1,
      }}
    >
      <Suspense fallback='loading'>
        <NotificationsSystem
          notifications={notifications}
          dismissNotification={dismissNotification}
          theme={wyboTheme}
        />
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </Suspense>
    </Box>
  )
}
