import { RotateLeft } from '@mui/icons-material'
import { Box, IconButton, TextField, Typography } from '@mui/material'
import React, {
  ChangeEvent,
  FC,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import { useNavigate } from 'react-router'
import { useNotifications } from 'reapop'
import { FlexContainer } from '../../components/atoms/FlexContainer'
import { useSocket } from '../../hooks/useSocket'
import { notificationMessage } from '../../utils/notificationMessage'

const emptyCode = Array.from({ length: 4 }).fill('') as string[]

export const JoinRoom: FC = () => {
  /*
   * Component state
   */
  const [code, setCode] = useState(emptyCode)

  const firstEl = useRef<HTMLDivElement>()
  const secondEl = useRef<HTMLDivElement | null>()
  const thirdEl = useRef<HTMLDivElement | null>()
  const fourthEl = useRef<HTMLDivElement | null>()

  const refs = [firstEl, secondEl, thirdEl, fourthEl]

  /*
   * Custom & 3th party hooks
   */

  const navigate = useNavigate()
  const { socket } = useSocket()
  const { notify } = useNotifications()

  /*
   * Methods
   */
  const handleChange =
    (index: number) =>
    (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>): void => {
      setCode(prev => {
        const next = [...prev]
        const value = event.target.value[0].toUpperCase()

        if (!/[A-Z]/i.exec(value)) return prev
        next[index] = value

        return next.map((val, valIndex) => (valIndex > index ? '' : val))
      })
    }

  const fullCode = useMemo(
    (): string => code.filter(val => val !== '').join(''),
    [code]
  )

  const focusInputField = useCallback((): void => {
    if (fullCode.length >= refs.length) return

    refs[fullCode.length].current?.focus()
  }, [fullCode])

  /*
   * Side effects
   */
  useEffect(() => {
    if (fullCode.length < refs.length) return focusInputField()

    socket?.emit('room:join:request', fullCode)
  }, [socket, focusInputField, fullCode])

  useEffect(() => {
    if (!socket) return

    socket.on('room:join:success', room => {
      notify({
        message: notificationMessage(
          'Whooo, add your anthems and get those feet moving!',
          '🩰'
        ),
      })
      navigate(`/session/${room.pin}`)
    })
    socket.on('room:join:not-found', pin => {
      setCode(emptyCode)
      notify({
        message: notificationMessage(
          `Oops... We could not find the session with <strong>${pin}</strong> as code`,
          '🕵️'
        ),
      })
    })

    return () => {
      socket.off('room:join:success')
      socket.off('room:join:not-found')
    }
  }, [socket])
  /*
   * Return
   */
  return (
    <FlexContainer
      onClick={focusInputField}
      sx={{
        pb: 40,
        pt: 15,
      }}
    >
      <Typography variant='h5' align='center'>
        Enter the session code of the fissa you want to join.
      </Typography>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: 'repeat(4, 1fr)',
          columnGap: 3,
          px: 2,
          '& input': {
            // ...theme.typography.h2,
            fontSize: '56px',
            fontWeight: 700,
            textAlign: 'center',
            px: 1.5,
            color: 'primary.light',
          },
        }}
      >
        {refs.map((ref, index) => (
          <TextField
            autoFocus={index === code.filter(Boolean).length}
            key={index}
            inputRef={ref}
            value={code[index]}
            onSelect={focusInputField}
            type='text'
            onChange={handleChange(index)}
          />
        ))}
      </Box>
      <Box display='flex' justifyContent='center'>
        <IconButton size='large' onClick={() => setCode(emptyCode)}>
          <RotateLeft fontSize='large' />
        </IconButton>
      </Box>
    </FlexContainer>
  )
}
