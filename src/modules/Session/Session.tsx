import { Box, Container, Typography } from '@mui/material'
import { FC } from 'react'
import { useParams } from 'react-router-dom'
import { RouteHeader } from '../../components/atoms/RouteHeader'
import { ActiveTrack } from '../../components/molecules/ActiveTrack'
import { TrackQueue } from '../../components/organisms/TrackQueue'
import { TrackManager } from './components/TrackManager'
import { QueueManager } from './components/QueueManager'
import { RoomCode } from './components/RoomCode'
import { RoomManager } from './components/RoomManager'

export const Session: FC = () => {
  /*
   * Component state
   */
  const { pin } = useParams<{ pin?: string }>()

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */
  if (!pin) return null

  return (
    <Container
      sx={{
        pt: 13,
        pb: 10,
      }}
    >
      <RouteHeader />
      <QueueManager />
      <RoomManager pin={pin} />
      <TrackManager />
      <Box mb={5.5}>
        <Box display='flex' justifyContent='space-between' alignItems='center' mb={4}>
          <Typography variant='h2'>Now playing</Typography>
          <RoomCode pin={pin} />
        </Box>
        <ActiveTrack />
      </Box>
      <TrackQueue />
    </Container>
  )
}
