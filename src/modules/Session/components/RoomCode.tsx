import { Box, ToggleButton, ToggleButtonGroup, Typography } from '@mui/material'
import { FC } from 'react'
import { useNotifications } from 'reapop'
import { getThemeGradient } from '../../../providers/ThemeProvider'
import { writeTextToClipboardAsync } from '../../../utils/clipboard'
import { notificationMessage } from '../../../utils/notificationMessage'

interface RoomCodeProps {
  pin: string
}

export const RoomCode: FC<RoomCodeProps> = props => {
  /*
   * Component state
   */
  const { pin } = props

  /*
   * Custom & 3th party hooks
   */
  const { notify } = useNotifications()

  /*
   * Methods
   */
  const copyHandler = async () => {
    const copy = await writeTextToClipboardAsync(
      `Join my party at ${window.location.href} 💃🕺`
    )

    if (!copy) {
      notify({
        message: notificationMessage('Oops... something went wrong.', '🕵️'),
      })
      return
    }

    notify({
      message: notificationMessage('Session link copied!', '✂️'),
    })
  }

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <Box
      display='flex'
      justifyContent='flex-end'
      alignItems='center'
      onClick={copyHandler}
    >
      <Typography variant='body2' sx={{ mr: 1, opacity: 0.6 }}>
        Session code
      </Typography>
      <ToggleButtonGroup
        size='small'
        value={pin}
        exclusive
        sx={theme => ({
          py: 0.2,
          px: 0.5,
          background: getThemeGradient(theme.palette.primary.main),
        })}
      >
        {pin.split('').map((value, index) => (
          <ToggleButton
            sx={{
              px: 0.1,
              py: 0,
              border: 0,
              color: 'primary.dark',
            }}
            disableRipple
            disableFocusRipple
            disableTouchRipple
            disabled
            key={index}
            value={value}
          >
            <Typography
              variant='body2'
              fontWeight='bold'
              sx={{ color: 'primary.dark' }}
            >
              {value}
            </Typography>
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    </Box>
  )
}
