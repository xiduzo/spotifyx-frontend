import { FC, useEffect } from 'react'
import { useSocket } from '../../../hooks/useSocket'
import { Vote } from '../../../redux/reducer'

export const VoteManager: FC = () => {
  /*
   * Component state
   */

  /*
   * Custom & 3th party hooks
   */
  const { socket } = useSocket()

  /*
   * Methods
   */

  /*
   * Side effects
   */
  useEffect(() => {
    if (!socket?.connected) return

    socket.on('track:votes', (votes: Vote[]) => {
      //console.log(votes)
    })

    return () => {
      socket.off('track:votes')
    }
  }, [socket?.connected])

  /*
   * Return
   */

  return null
}
