import { ArrowBackOutlined, CloseOutlined } from '@mui/icons-material'
import {
  AppBar,
  Avatar,
  Box,
  Dialog,
  DialogProps,
  Hidden,
  IconButton,
  Slide,
  Toolbar,
  Typography,
} from '@mui/material'
import { TransitionProps } from '@mui/material/transitions'
import React, { FC, useCallback, useEffect, useRef, useState } from 'react'
import { BottomSheet } from '../../../components/atoms/BottomSheet'
import { DarkPrimaryButton } from '../../../components/atoms/DarkPrimaryButton'
import { EmptyState } from '../../../components/atoms/EmptyState'
import { PlaylistsList } from '../../../components/organisms/PlaylistList'
import { TrackList } from '../../../components/organisms/TrackList'
import { useSpotify } from '../../../hooks/useSpotify'

interface TrackSelectionProps extends Omit<DialogProps, 'onClose'> {
  onClose: (trackUris?: string[]) => void
}

const Transition = React.forwardRef(
  (
    props: TransitionProps & {
      children: React.ReactElement
    },
    ref: React.Ref<unknown>
  ) => <Slide direction='up' ref={ref} {...props} />
)

export const TrackSelection: FC<TrackSelectionProps> = props => {
  /*
   * Component state
   */
  const { onClose, ...dialogProps } = props

  const [playlists, setPlaylists] = useState<SpotifyApi.PlaylistObjectSimplified[]>([])
  const [tracks, setTracks] = useState<SpotifyApi.TrackObjectFull[]>([])
  const [selectedPlaylist, setSelectedPlaylist] =
    useState<SpotifyApi.PlaylistObjectSimplified | null | undefined>()
  const [selectedTracks, setSelectedTracks] = useState<string[]>([])
  const dialogRef = useRef<HTMLDivElement | null>(null)

  /*
   * Custom & 3th party hooks
   */
  const { getUserPlayLists, getPlaylistTracks } = useSpotify()

  /*
   * Methods
   */
  const clearState = (): void => {
    deselectPlaylist()
    setSelectedTracks([])
  }

  const handleClose = (): void => {
    onClose()
    clearState()
  }

  const handleAdd = (): void => {
    onClose(selectedTracks)
    clearState()
  }

  const handleBack = (): void => {
    dialogRef.current?.scrollTo(0, 0)
    if (selectedPlaylist) {
      deselectPlaylist()
      return
    }

    handleClose()
  }

  const selectPlaylist =
    (playlist: SpotifyApi.PlaylistObjectSimplified) => (): void => {
      setSelectedPlaylist(playlist)
    }
  const deselectPlaylist = (): void => setSelectedPlaylist(null)

  const toggleTrackSelection = useCallback(
    (track: SpotifyApi.TrackObjectFull) => (): void => {
      setSelectedTracks(prevState => {
        if (prevState.includes(track.uri)) {
          return prevState.filter(_track => _track !== track.uri)
        }
        return [...prevState, track.uri]
      })
    },
    []
  )

  /*
   * Side effects
   */
  useEffect(() => {
    if (!dialogProps.open) return

    getUserPlayLists().then(setPlaylists)
  }, [dialogProps.open])

  useEffect(() => {
    if (!selectedPlaylist) {
      setTracks([])
      return
    }

    getPlaylistTracks(selectedPlaylist.id).then(setTracks)
  }, [selectedPlaylist])

  /*
   * Return
   */

  return (
    <Dialog {...dialogProps} fullScreen TransitionComponent={Transition}>
      {/* TODO: make component, same as RouteHeader */}
      <AppBar elevation={0}>
        <Toolbar
          sx={theme => ({
            minHeight: theme.spacing(13),
          })}
        >
          <IconButton
            edge='start'
            color='inherit'
            onClick={handleBack}
            aria-label='close'
          >
            <ArrowBackOutlined />
          </IconButton>
          <Typography
            variant='h4'
            textAlign='center'
            flexGrow={1}
            ml={Number(!selectedPlaylist) * 3}
          >
            {selectedPlaylist ? selectedPlaylist.name : 'Your playlists'}
          </Typography>
          <IconButton
            edge='end'
            color='inherit'
            onClick={handleClose}
            aria-label='close'
          >
            <CloseOutlined />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Box px={2} pt={13} pb={selectedTracks.length > 0 ? 30 : 0} ref={dialogRef}>
        {/* https://github.com/mui/material-ui/issues/9186 */}
        <Hidden xsUp={!!selectedPlaylist}>
          {!playlists.length && (
            <EmptyState emoji='🦹' title='Stealing your playlist data' />
          )}
          <PlaylistsList playlists={playlists} onPlaylistClick={selectPlaylist} />
        </Hidden>
        <Hidden xsUp={!selectedPlaylist}>
          <Avatar
            src={selectedPlaylist?.images[0]?.url}
            variant='rounded'
            sx={{ mb: 3, width: '175px', height: '175px' }}
          />
          <Typography variant='h1' gutterBottom>
            {selectedPlaylist?.name}
          </Typography>
          {!tracks.length && <EmptyState emoji='🎶' title='Fetching tracks data' />}
          <TrackList tracks={tracks} onTrackClick={toggleTrackSelection} />
        </Hidden>
      </Box>
      <BottomSheet
        in
        subtitle={`${selectedTracks.length} track${
          selectedTracks.length === 1 ? '' : 's'
        } selected`}
      >
        <DarkPrimaryButton disabled={!selectedTracks.length} onClick={handleAdd}>
          Add to queue
        </DarkPrimaryButton>
      </BottomSheet>
    </Dialog>
  )
}
