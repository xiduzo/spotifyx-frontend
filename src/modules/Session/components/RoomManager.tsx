import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'
import { useNotifications } from 'reapop'
import { useSocket } from '../../../hooks/useSocket'
import { updateRoom } from '../../../redux/actions'
import { RootState } from '../../../redux/reducer'
import { notificationMessage } from '../../../utils/notificationMessage'

interface RoomManagerProps {
  pin: string
}

export const RoomManager: FC<RoomManagerProps> = props => {
  /*
   * Component state
   */
  const { pin } = props

  /*
   * Custom & 3th party hooks
   */
  const { socket } = useSocket()
  const { notify } = useNotifications()
  const navigate = useNavigate()

  const dispatch = useDispatch()

  /*
   * Methods
   */

  /*
   * Side effects
   */

  // Joining room
  useEffect(() => {
    if (!socket) return

    socket.on('room:join:success', (room: RootState) => {
      dispatch(updateRoom(room))
    })

    socket.on('room:join:not-found', () => {
      notify({
        message: notificationMessage(
          `Oops... We could not find the session with <strong>${pin}</strong> as code`,
          '❌'
        ),
      })
      navigate('/')
    })

    socket.emit('room:join:request', pin)

    return () => {
      socket.off('room:join:success')
      socket.off('room:join:not-found')
    }
  }, [socket, pin, dispatch])

  /*
   * Return
   */
  return null
}
