import AddIcon from '@mui/icons-material/Add'
import { Box, Fab, FabProps, Hidden, Zoom } from '@mui/material'
import { FC, Fragment, useCallback, useEffect, useMemo, useState } from 'react'
import { useNotifications } from 'reapop'
import { BottomSheet } from '../../../components/atoms/BottomSheet'
import { DarkPrimaryButton } from '../../../components/atoms/DarkPrimaryButton'
import { Track } from '../../../components/atoms/Track'
import { useSocket } from '../../../hooks/useSocket'
import { useSpotify } from '../../../hooks/useSpotify'
import { TransitionDuration } from '../../../utils/animation'
import { readTextFromClipboardAsync } from '../../../utils/clipboard'
import { notificationMessage } from '../../../utils/notificationMessage'
import { TrackSelection } from './TrackSelection'

enum Stage {
  Init,
  PasteLink,
  SelectFromPlaylist,
}

export const TrackManager: FC<FabProps> = props => {
  /*
   * Component state
   */
  const { ...fabProps } = props

  const [opened, setOpened] = useState(false)
  const [stage, setStage] = useState(Stage.Init)
  const [trackToAdd, setTrackToAdd] = useState<SpotifyApi.TrackObjectFull>()

  const subtitle = useMemo((): string => {
    switch (stage) {
      case Stage.PasteLink:
        if (trackToAdd) return 'Spotify song link found!'
        return 'Copy a Spotify song link and come back'
      default:
        return 'Copy a Spotify song link or browse in your Spotify playlists'
    }
  }, [stage, trackToAdd])

  /*
   * Custom & 3th party hooks
   */
  const { getTracks } = useSpotify()
  const { notify } = useNotifications()

  const { socket } = useSocket()

  /*
   * Methods
   */
  const closeBottomSheet = (): void => {
    setTrackToAdd(undefined)
    setStage(Stage.Init)
    setOpened(false)
  }

  const openBottomSheet = (): void => setOpened(true)
  const trackSelectionHandler = (trackUris?: string[]): void => {
    closeBottomSheet()
    if (!trackUris) return
    if (trackUris?.length <= 0) return

    socket?.emit('room:tracks:add', trackUris)

    const hands = ['🤘', '🤙', '👌', '👊', '✌️']

    notify({
      message: notificationMessage(
        `You've added <strong>${trackUris.length}</strong/> track${
          trackUris.length === 1 ? '' : 's'
        } to the queue. Kick it!`,
        hands[Math.floor(Math.random() * hands.length)]
      ),
    })
  }

  const handlePasteLink = useCallback(async (): Promise<void> => {
    if (trackToAdd) return

    try {
      const text = await readTextFromClipboardAsync()
      const trackRegex = '([a-zA-Z0-9]){22}'
      const isSpotifyTrack = new RegExp(
        `https://open.spotify.com/track/${trackRegex}`,
        'gm'
      ).exec(text)

      if (!isSpotifyTrack) return

      const trackId = new RegExp(trackRegex).exec(isSpotifyTrack[0])
      if (!trackId) return

      getTracks([trackId[0]])
        .then(tracks => setTrackToAdd(tracks[0]))
        .catch(() => {
          notify({
            message: notificationMessage(
              'Oops... Something went wrong. Please try again.',
              '❌'
            ),
          })
        })
    } catch {
      // console.error('error handlePasteLink')
    }
  }, [getTracks, notify])

  const changeStage = (newStage: Stage) => (): void => setStage(newStage)
  const removeTrackToAdd = (): void => setTrackToAdd(undefined)

  const handleBack = (): void => {
    switch (stage) {
      case Stage.PasteLink:
        setStage(Stage.Init)
        return
      default:
        return
    }
  }

  const gotoSpotify = (): void => {
    window?.open('https://open.spotify.com', '_blank')?.focus()
  }

  const addTrack = (): void => {
    closeBottomSheet()

    if (!trackToAdd) return

    socket?.emit('room:tracks:add', [trackToAdd.uri])
  }

  /*
   * Side effects
   */
  useEffect(() => {
    switch (stage) {
      case Stage.PasteLink:
        handlePasteLink()
        return
      default:
        return
    }
  }, [handlePasteLink, stage])

  useEffect(() => {
    window.addEventListener('focus', handlePasteLink)

    return () => {
      window.removeEventListener('focus', handlePasteLink)
    }
  }, [handlePasteLink])

  /*
   * Return
   */
  return (
    <Fragment>
      <Zoom
        in={!opened}
        timeout={{
          enter: TransitionDuration.enteringScreen,
          exit: TransitionDuration.shortest,
        }}
      >
        <Fab {...fabProps} onClick={openBottomSheet} aria-label='Add track'>
          <AddIcon />
        </Fab>
      </Zoom>
      <BottomSheet
        in={opened}
        backDrop
        closeOnBackdropClick={stage !== Stage.SelectFromPlaylist}
        onClose={closeBottomSheet}
        onBack={stage !== Stage.Init ? handleBack : undefined}
        title='Add tracks'
        subtitle={subtitle}
      >
        <Box>
          <Hidden xsUp={stage !== Stage.Init}>
            <DarkPrimaryButton
              sx={{ mb: 3 }}
              onClick={changeStage(Stage.SelectFromPlaylist)}
            >
              Browse my playlists
            </DarkPrimaryButton>
            <DarkPrimaryButton onClick={changeStage(Stage.PasteLink)}>
              Paste a song link
            </DarkPrimaryButton>
          </Hidden>
          <Hidden xsUp={stage !== Stage.PasteLink}>
            <Track
              onClick={!trackToAdd ? handlePasteLink : undefined}
              track={
                trackToAdd ??
                ({
                  name: 'Paste a song link',
                  artists: [{ name: 'Click to paste spotify link' }],
                } as SpotifyApi.TrackObjectFull)
              }
              outlined
              sx={{ mb: 4.75, opacity: !trackToAdd ? 0.5 : 1 }}
              onAbort={removeTrackToAdd}
            />
            <Hidden xsUp={!!trackToAdd}>
              <DarkPrimaryButton onClick={gotoSpotify}>Open spotify</DarkPrimaryButton>
            </Hidden>
            <Hidden xsUp={!trackToAdd}>
              <DarkPrimaryButton onClick={addTrack}>Add to queue</DarkPrimaryButton>
            </Hidden>
          </Hidden>
        </Box>
      </BottomSheet>
      <TrackSelection
        open={stage === Stage.SelectFromPlaylist}
        onClose={trackSelectionHandler}
      />
    </Fragment>
  )
}
