import { FC, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useDebounce } from '../../../hooks/useDebounce'
import { useSocket } from '../../../hooks/useSocket'
import { useSpotify } from '../../../hooks/useSpotify'
import { addTracksToLocalDb, setProgress, setQueue } from '../../../redux/actions'
import { selectMissingTracks } from '../../../redux/selectors'

export const QueueManager: FC = () => {
  /*
   * Component state
   */
  const _missingTracks = useSelector(selectMissingTracks)

  /*
   * Custom & 3th party hooks
   */
  const { socket } = useSocket()
  const dispatch = useDispatch()
  const { getTracks } = useSpotify()

  /*
   * Methods
   */

  /*
   * Side effects
   */
  useDebounce(_missingTracks, 500, tracksToFetch => {
    if (tracksToFetch.length === 0) return
    getTracks(tracksToFetch).then(tracks => {
      dispatch(addTracksToLocalDb(tracks))
    })
  })

  // Queue
  useEffect(() => {
    // TODO listen to general room update in a central place
    socket?.on('room:queue', trackUris => {
      dispatch(setQueue(trackUris))
    })

    socket?.on('room:track:progress', progress => {
      dispatch(setProgress(progress))
    })

    return () => {
      socket?.off('room:queue')
      socket?.off('room:track:progress')
    }
  }, [dispatch, socket])

  /*
   * Return
   */

  return null
}
