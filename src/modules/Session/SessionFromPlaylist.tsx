import { Box, Container } from '@mui/material'
import { FC, useCallback, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useNotifications } from 'reapop'
import { BottomSheet } from '../../components/atoms/BottomSheet'
import { DarkPrimaryButton } from '../../components/atoms/DarkPrimaryButton'
import { EmptyState } from '../../components/atoms/EmptyState'
import { Playlist } from '../../components/atoms/Playlist'
import { RouteHeader } from '../../components/atoms/RouteHeader'
import { PlaylistsList } from '../../components/organisms/PlaylistList'
import { useSocket } from '../../hooks/useSocket'
import { useSpotify } from '../../hooks/useSpotify'
import { notificationMessage } from '../../utils/notificationMessage'

export const SessionFromPlaylist: FC = () => {
  /**
   * Component state
   */
  const [playlists, setPlaylists] = useState<SpotifyApi.PlaylistObjectSimplified[]>([])
  const [selectedPlaylist, setSelectedPlaylist] =
    useState<SpotifyApi.PlaylistObjectSimplified | undefined>()
  const [creatingPlaylist, setCreatingPlaylist] = useState(false)

  /*
   * Custom & 3th party hooks
   */
  const { socket } = useSocket()
  const navigate = useNavigate()
  const { notify } = useNotifications()
  const { userId, getUserPlayLists } = useSpotify()

  /**
   * Methods
   */
  // const selectPlaylist = useCallback(
  //   (playlist: SpotifyApi.PlaylistObjectSimplified) => (): void => {
  //     setSelectedPlaylist(playlist)
  //   },
  //   []
  // )

  const selectPlaylist = useCallback(
    (playlist: SpotifyApi.PlaylistObjectSimplified) => (): void => {
      setSelectedPlaylist(playlist)
    },
    []
  )

  const deselectPlaylist = (): void => setSelectedPlaylist(undefined)

  const createRoom = (): void => {
    if (!selectedPlaylist) {
      notify({
        message: notificationMessage(
          'Oops... Something went wrong. Please select your playlist again.',
          '❌'
        ),
      })
      return
    }

    setCreatingPlaylist(true)

    socket?.emit('room:creation:request', selectedPlaylist.id)
  }

  /*
   * Side effects
   */
  useEffect(() => {
    if (!userId) return
    if (!socket) return

    const errorHandler = () => {
      notify({
        message: notificationMessage(
          'Oops... We can not fetch your playlists. Restart the app or try again later.',
          '❌'
        ),
      })
      navigate(-1)
    }

    getUserPlayLists().then(setPlaylists).catch(errorHandler)
  }, [socket, userId])

  useEffect(() => {
    socket?.on('room:creation:success', room => {
      socket.off('room:creation:success')
      notify({
        message: notificationMessage(
          'Aye, your playlist has been imported successfully!',
          '🎉'
        ),
      })
      navigate(`/session/${room.pin}`)
    })

    socket?.on('room:creation:error', error => {
      notify({
        message: notificationMessage((error as Error).message, '❌'),
      })
    })

    return () => {
      socket?.off('room:creation:success')
      socket?.off('room:creation:error')
    }
  }, [socket, navigate])

  /**
   * Render
   */
  return (
    <Container sx={{ pt: 13 }}>
      <RouteHeader currentRouteTitle='Select playlist' />
      {!playlists.length && (
        <EmptyState emoji='🦹' title='Stealing your playlist data' />
      )}
      <PlaylistsList playlists={playlists} onPlaylistClick={selectPlaylist} />
      <BottomSheet
        in={!!selectedPlaylist}
        backDrop
        onClose={deselectPlaylist}
        title='Your group session will start based on'
      >
        <Box>
          <Playlist playlist={selectedPlaylist} sx={{ mb: 3 }} outlined />
          <DarkPrimaryButton onClick={createRoom} disabled={creatingPlaylist}>
            Let's kick it!
          </DarkPrimaryButton>
        </Box>
      </BottomSheet>
    </Container>
  )
}
