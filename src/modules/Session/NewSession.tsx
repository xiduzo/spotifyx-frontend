import { Box } from '@mui/material'
import { FC, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useNotifications } from 'reapop'
import { FlexContainer } from '../../components/atoms/FlexContainer'
import { PageText } from '../../components/atoms/PageText'
import { PrimaryButton } from '../../components/atoms/PrimaryButton'
import { useSocket } from '../../hooks/useSocket'
import { notificationMessage } from '../../utils/notificationMessage'

export const NewSession: FC = () => {
  /*
   * Component state
   */

  /*
   * Custom & 3th party hooks
   */
  const navigate = useNavigate()
  const { socket } = useSocket()
  const { notify } = useNotifications()

  /*
   * Methods
   */
  const createEmptyRoom = (): void => {
    socket?.emit('room:creation:request')
  }

  const selectPlaylist = (): void => {
    navigate('playlist')
  }

  useEffect(() => {
    socket?.on('room:creation:success', room => {
      socket.off('room:creation:success')
      notify({
        message: notificationMessage(
          'Your party is ready, add some anthems to get the party started!',
          '🎉'
        ),
      })
      navigate(`/session/${room.pin}`)
    })

    return () => {
      socket?.off('room:creation:success')
    }
  }, [socket])

  /*
   * Render
   */
  return (
    <FlexContainer>
      <PageText title='Create group session' subtitle='How would you like to start?' />
      <Box display='flex' flexDirection='column' width='inherit' mt={4}>
        <Box mb={3}>
          <PrimaryButton onClick={selectPlaylist}>Select a playlist</PrimaryButton>
        </Box>
        <PrimaryButton onClick={createEmptyRoom}>Start from blank</PrimaryButton>
      </Box>
    </FlexContainer>
  )
}
