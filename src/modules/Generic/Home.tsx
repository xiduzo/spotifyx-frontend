import { Box } from '@mui/material'
import { FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { FlexContainer } from '../../components/atoms/FlexContainer'
import { PageText } from '../../components/atoms/PageText'
import { PrimaryButton } from '../../components/atoms/PrimaryButton'
import { useSpotify } from '../../hooks/useSpotify'

export const Home: FC = () => {
  /*
   * Component state
   */

  /*
   * Custom & 3th party hooks
   */
  const navigate = useNavigate()
  const { userId, spotifyAuth } = useSpotify()

  /*
   * Methods
   */
  const newSession = () => navigate('/new')
  const joinSession = () => navigate('/join')

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <FlexContainer hideBackButton>
      <PageText
        title='Ola ola...'
        subtitle='Do you want to create or join a group session?'
      />
      <Box display='flex' flexDirection='column' width='inherit'>
        <Box mb={3}>
          {userId && (
            <PrimaryButton onClick={newSession}>Create group session</PrimaryButton>
          )}
          {!userId && (
            <PrimaryButton onClick={spotifyAuth}>
              Sign in to create a session
            </PrimaryButton>
          )}
        </Box>
        <PrimaryButton onClick={joinSession}>Join group session</PrimaryButton>
      </Box>
    </FlexContainer>
  )
}
