import { FC } from 'react'
import { useRoutes } from 'react-router-dom'
import { Home } from './Generic/Home'
import { JoinRoom } from './Session/Join'
import { NewSession } from './Session/NewSession'
import { SessionFromPlaylist } from './Session/SessionFromPlaylist'
import { Session } from './Session/Session'

export const Routes: FC = () => {
  const routes = useRoutes([
    {
      path: '/',
      element: <Home />,
      caseSensitive: true,
    },
    {
      path: '/session/:pin',
      element: <Session />,
      caseSensitive: true,
    },
    {
      path: '/new',
      element: <NewSession />,
      caseSensitive: true,
    },
    {
      path: '/join',
      element: <JoinRoom />,
    },
    {
      path: '/new/playlist',
      element: <SessionFromPlaylist />,
    },
    { path: '*', element: Session },
  ])

  return routes
}
