import { Box, BoxProps } from '@mui/material'
import { FC, memo } from 'react'
import { useSelector } from 'react-redux'
import { selectTrack } from '../../redux/selectors'
import { Track } from '../atoms/Track'

interface QueuedTrackProps extends Omit<BoxProps, 'onClick'> {
  track: string
  onClick?: (track?: SpotifyApi.TrackObjectFull) => void
}

export const QueuedTrack: FC<QueuedTrackProps> = memo(props => {
  /*
   * Component state
   */
  const { track, onClick, ...boxProps } = props

  const _track = useSelector(selectTrack(track))

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */
  const selectTrackHandler = (): void => {
    if (onClick) onClick(_track)
  }

  /*
   * Side effects
   */

  /*
   * Return
   */
  return (
    <Box display='flex' {...boxProps} onClick={selectTrackHandler}>
      <Track track={_track} />
    </Box>
  )
})
