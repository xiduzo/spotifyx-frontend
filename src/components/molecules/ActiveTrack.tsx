import { List } from '@mui/material'
import React, { FC } from 'react'
import { useSelector } from 'react-redux'
import { selectActiveTrack, selectProgress, selectTrack } from '../../redux/selectors'
import { Track } from '../atoms/Track'

export const ActiveTrack: FC = () => {
  /*
   * Component state
   */
  const _activeTrack = useSelector(selectActiveTrack)
  const _track = useSelector(selectTrack(_activeTrack))
  const _progress = useSelector(selectProgress)

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */
  return (
    <List sx={{ p: 0 }}>
      <Track
        track={_track}
        size='large'
        disableRipple
        disableTouchRipple
        progress={_progress}
        sx={{ p: 0 }}
      />
    </List>
  )
}
