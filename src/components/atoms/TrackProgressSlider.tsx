import { Slider, SliderProps } from '@mui/material'
import { FC, useEffect, useState } from 'react'

interface TrackProgressSliderProps extends SliderProps {
  duration?: number
  progress?: number
}

export const TrackProgressSlider: FC<TrackProgressSliderProps> = props => {
  /*
   * Component state
   */
  const { duration = 0, progress = 0, ...sliderProps } = props

  const [localProgress, setLocalProgress] = useState(progress)

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */
  useEffect(() => {
    setLocalProgress(progress)

    const start = performance.now()
    const interval = setInterval(() => {
      const now = performance.now()
      const diff = now - start
      setLocalProgress(progress + diff)
    }, 500)

    return () => {
      clearInterval(interval)
    }
  }, [progress])

  /*
   * Return
   */

  return (
    <Slider
      {...sliderProps}
      sx={{
        '& .MuiSlider-thumb': {
          display: 'none',
        },
      }}
      min={0}
      max={duration}
      value={localProgress}
      defaultValue={progress}
    />
  )
}
