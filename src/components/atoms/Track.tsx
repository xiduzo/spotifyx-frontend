import { CloseOutlined, MoreVertOutlined } from '@mui/icons-material'
import {
  alpha,
  Avatar,
  Box,
  IconButton,
  ListItemAvatar,
  ListItemButton,
  ListItemButtonProps,
  Theme,
  Typography,
  Zoom,
} from '@mui/material'
import { FC, memo, MouseEvent, useState } from 'react'
import { Vote } from '../../redux/reducer'
import { TrackProgressSlider } from './TrackProgressSlider'
import { TrackTitle } from './TrackTitle'

type TrackSize = 'normal' | 'large' | undefined
interface TrackProps extends ListItemButtonProps {
  track?: SpotifyApi.TrackObjectFull
  // Can be positive or negative
  votesTotal?: number
  myVote?: Vote
  progress?: number
  size?: TrackSize
  selectable?: boolean
  outlined?: boolean
}

export const Track: FC<TrackProps> = memo(({ size = 'normal', ...props }) => {
  /*
   * Component state
   */
  const { track, progress, selectable, votesTotal, outlined, ...listItemButtonProps } =
    props

  const [isSelected, setIsSelected] = useState(false)

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */
  const clickHandler = (event: MouseEvent<HTMLDivElement>) => {
    if (listItemButtonProps.onClick) listItemButtonProps.onClick(event)

    if (selectable) setIsSelected(!isSelected)
  }

  const abortHandler = (event: any) => {
    if (listItemButtonProps.onAbort) listItemButtonProps.onAbort(event)

    if (selectable) setIsSelected(false)
  }

  const avatarBackground = (theme: Theme): string => {
    if (!track?.id) return theme.palette.primary.dark

    const opacity = isSelected ? 0.4 : 0.1
    const color = alpha(theme.palette.primary.main, opacity)
    return `linear-gradient(${color}, ${color}), url(${track?.album?.images[0]?.url})`
  }

  /*
   * Side effects
   */

  /*
   * Return
   */
  return (
    // todo: gesture based voting
    // See https://www.figma.com/file/86TTfExhFJFfpWipqBZsNL/Mobile?node-id=867%3A26812
    <ListItemButton
      disableGutters
      {...listItemButtonProps}
      onClick={clickHandler}
      selected={isSelected}
      // @ts-ignore
      sx={theme => ({
        py: outlined ? 0 : 1,
        borderStyle: 'solid',
        borderWidth: outlined ? '2px' : '0px',
        borderColor: alpha(theme.palette.primary.dark, 0.1),
        // borderRadius: theme.shape.borderRadius,
        '&.Mui-selected': {
          background: 'transparent',
          '&:hover': {
            background: 'transparent',
          },
          '.MuiListItemText-root': {
            opacity: 0.4,
          },
        },
        ...listItemButtonProps.sx,
      })}
      // TODO: gesture based voting
      // onTouchStart={console.log}
      // onMouseDown={console.log}
    >
      <ListItemAvatar>
        <Avatar
          variant='rounded'
          // src={track.album.images[0]?.url}
          sx={theme => ({
            boxShadow: size === 'large' ? 6 : 3,
            width: size === 'large' ? '125px' : '80px',
            height: size === 'large' ? '125px' : '80px',
            mr: 2,
            background: `${avatarBackground(theme)}`,
            backgroundSize: 'cover',
            transform: `scale(${outlined ? 1.05 : 1})`,
            borderTopRightRadius: outlined ? 0 : '12px',
            borderBottomRightRadius: outlined ? 0 : '12px',
          })}
        >
          <Zoom in={isSelected}>
            <Typography variant='h1'>✅</Typography>
          </Zoom>
        </Avatar>
      </ListItemAvatar>
      <Box flexGrow={1}>
        <TrackTitle track={track} />
        {progress !== undefined && (
          <TrackProgressSlider duration={track?.duration_ms} progress={progress} />
        )}
      </Box>
      {votesTotal && <MoreVertOutlined />}
      {track?.id && listItemButtonProps.onAbort && (
        <IconButton sx={{ color: 'primary.dark' }} onClick={abortHandler}>
          <CloseOutlined />
        </IconButton>
      )}
    </ListItemButton>
  )
})
