import { Container, ContainerProps } from '@mui/material'
import React, { FC } from 'react'
import { RouteHeader } from './RouteHeader'

interface FlexContainerProps extends ContainerProps {
  hideBackButton?: boolean
}

export const FlexContainer: FC<FlexContainerProps> = props => {
  /*
   * Component state
   */
  const { hideBackButton, children, ...containerProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <Container
      {...containerProps}
      maxWidth='xs'
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        minHeight: '100%',
        pt: 40,
        ...containerProps.sx,
      }}
    >
      <RouteHeader hideBackButton={hideBackButton} />
      {children}
    </Container>
  )
}
