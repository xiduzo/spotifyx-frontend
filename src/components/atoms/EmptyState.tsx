import { Box, Typography } from '@mui/material'
import { BoxProps } from '@mui/system'
import React, { FC } from 'react'

interface EmptyStateProps extends BoxProps {
  emoji: string
  title: string
}

export const EmptyState: FC<EmptyStateProps> = props => {
  /*
   * Component state
   */
  const { emoji, title, ...boxProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <Box
      sx={{
        height: '66vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        ...boxProps.sx,
      }}
    >
      <Typography align='center' fontSize='144px'>
        {emoji}
      </Typography>
      <Typography align='center' variant='h5'>
        {title}
      </Typography>
    </Box>
  )
}
