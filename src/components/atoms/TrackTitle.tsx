import { ListItemText, ListItemTextProps, Skeleton } from '@mui/material'
import { FC } from 'react'

interface TrackTitleProps extends ListItemTextProps {
  track?: SpotifyApi.TrackObjectFull
}

export const TrackTitle: FC<TrackTitleProps> = props => {
  /*
   * Component state
   */
  const { track } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <ListItemText
      primary={track?.name ?? <Skeleton />}
      secondary={track?.artists?.map(artist => artist.name).join(', ') ?? <Skeleton />}
    />
  )
}
