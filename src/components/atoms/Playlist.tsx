import { MusicNoteRounded } from '@mui/icons-material'
import {
  alpha,
  Avatar,
  ListItemAvatar,
  ListItemButton,
  ListItemButtonProps,
  ListItemText,
  Skeleton,
  Typography,
} from '@mui/material'
import React, { FC, Fragment } from 'react'

interface PlaylistProps extends ListItemButtonProps {
  playlist?: SpotifyApi.PlaylistObjectSimplified
  outlined?: boolean
}

export const Playlist: FC<PlaylistProps> = props => {
  /*
   * Component state
   */
  const { playlist, outlined, ...listItemButtonProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <ListItemButton
      {...listItemButtonProps}
      // @ts-ignore
      sx={theme => ({
        px: 0,
        py: outlined ? 0 : 1,
        borderStyle: 'solid',
        borderWidth: outlined ? '2px' : '0px',
        borderColor: alpha(theme.palette.primary.dark, 0.1),
        borderRadius: theme.shape.borderRadius,
        ...listItemButtonProps.sx,
      })}
    >
      <ListItemAvatar>
        <Avatar
          sx={theme => ({
            width: '80px',
            height: '80px',
            mr: 2.5,
            transform: `scale(${outlined ? 1.05 : 1})`,
            borderTopRightRadius: outlined ? 0 : theme.shape.borderRadius,
            borderBottomRightRadius: outlined ? 0 : theme.shape.borderRadius,
          })}
          src={playlist?.images[0]?.url ?? ''}
          variant='rounded'
        >
          {playlist ? (
            playlist.images?.length <= 0 && <MusicNoteRounded />
          ) : (
            <Skeleton />
          )}
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={playlist?.name ?? <Skeleton />}
        secondary={
          <Fragment>
            {playlist ? (
              <Typography variant='caption'>
                {playlist.owner.display_name} • {playlist.tracks.total} tracks
              </Typography>
            ) : (
              <Skeleton />
            )}
          </Fragment>
        }
      />
    </ListItemButton>
  )
}
