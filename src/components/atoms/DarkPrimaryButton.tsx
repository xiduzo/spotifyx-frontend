import { ButtonProps } from '@mui/material'
import React, { FC } from 'react'
import { PrimaryButton } from './PrimaryButton'

export const DarkPrimaryButton: FC<ButtonProps> = props => {
  /*
   * Component state
   */
  const { children, ...buttonProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <PrimaryButton
      {...buttonProps}
      sx={{
        backgroundColor: 'primary.dark',
        color: 'primary.contrastText',
        '&:hover': {
          backgroundColor: 'primary.dark',
        },
        '&.Mui-disabled': {
          backgroundColor: 'primary.dark',
          opacity: 0.5,
        },
        ...buttonProps.sx,
      }}
    >
      {children}
    </PrimaryButton>
  )
}
