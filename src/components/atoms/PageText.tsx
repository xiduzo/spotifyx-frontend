import { Box, Typography } from '@mui/material'
import { BoxProps } from '@mui/system'
import React, { FC } from 'react'

interface PageTextProps extends BoxProps {
  title: string
  subtitle: string
}

export const PageText: FC<PageTextProps> = props => {
  /*
   * Component state
   */
  const { title, subtitle, ...boxProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <Box {...boxProps}>
      <Typography align='center' variant='h1' gutterBottom>
        {title}
      </Typography>
      <Typography align='center' variant='h5' component='h2'>
        {subtitle}
      </Typography>
    </Box>
  )
}
