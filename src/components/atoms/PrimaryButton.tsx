import { Button, ButtonProps } from '@mui/material'
import React, { FC } from 'react'

// interface PrimaryButtonProps extends ButtonProps {}

export const PrimaryButton: FC<ButtonProps> = props => {
  /*
   * Component state
   */
  const { children, ...buttonProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <Button {...buttonProps} sx={{ ...buttonProps.sx }} fullWidth variant='contained'>
      {children}
    </Button>
  )
}
