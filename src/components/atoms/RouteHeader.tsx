import { ArrowBackOutlined } from '@mui/icons-material'
import { AppBar, IconButton, Toolbar, Typography } from '@mui/material'
import { FC, useEffect } from 'react'
import { useNavigate } from 'react-router'

interface RouteHeaderProps {
  currentRouteTitle?: string
  gotoRoute?: string
  hideBackButton?: boolean
}
export const RouteHeader: FC<RouteHeaderProps> = props => {
  /**
   * Component state
   */
  const { currentRouteTitle, gotoRoute, hideBackButton = false } = props

  /**
   * Custom & 3th party hooks
   */
  const navigate = useNavigate()

  /**
   * Methods
   */
  const goBack = () => {
    if (gotoRoute) navigate(gotoRoute)

    navigate(-1)
  }

  /**
   * Side effects
   */
  useEffect(() => {
    const scrollEvent = (event: Event) => {
      // console.log(event, window.scrollY)
      // TODO: fancy header title animation
    }

    const wheelEvent =
      'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel'

    window.addEventListener('scroll', scrollEvent)
    window.addEventListener('DOMMouseScroll', scrollEvent) // older FF
    window.addEventListener(wheelEvent, scrollEvent) // modern desktop
    window.addEventListener('touchmove', scrollEvent) // mobile
    window.addEventListener('keydown', scrollEvent)

    return () => {
      window.removeEventListener('scroll', scrollEvent)
      window.removeEventListener('DOMMouseScroll', scrollEvent) // older FF
      window.removeEventListener(wheelEvent, scrollEvent) // modern desktop
      window.removeEventListener('touchmove', scrollEvent) // mobile
      window.removeEventListener('keydown', scrollEvent)
    }
  }, [])

  /**
   * Render
   */
  return (
    <AppBar elevation={0}>
      <Toolbar
        sx={theme => ({
          minHeight: theme.spacing(13),
        })}
      >
        {!hideBackButton && (
          <IconButton size='large' edge='start' aria-label='menu' onClick={goBack}>
            <ArrowBackOutlined />
          </IconButton>
        )}
        <Typography variant='h4' textAlign='center' flexGrow={1}>
          {currentRouteTitle}
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
