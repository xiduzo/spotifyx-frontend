import { ArrowBackOutlined, CloseOutlined } from '@mui/icons-material'
import {
  alpha,
  Box,
  IconButton,
  Paper,
  Slide,
  SlideProps,
  Typography,
} from '@mui/material'
import React, { FC, useRef } from 'react'
import { useClickOutside } from '../../hooks/useClickOutside'
import { getThemeGradient } from '../../providers/ThemeProvider'
import { v4 as uuid } from 'uuid'
import { TransitionDuration, TransitionEasing } from '../../utils/animation'

interface BottomSheetProps extends SlideProps {
  onBack?: () => void
  onClose?: () => void
  backDrop?: boolean
  closeOnBackdropClick?: boolean
  title?: string
  subtitle?: string
}

export const BottomSheet: FC<BottomSheetProps> = props => {
  /*
   * Component state
   */
  const {
    onBack,
    onClose,
    backDrop,
    closeOnBackdropClick,
    children,
    title,
    subtitle,
    ...slideProps
  } = props

  const sheetRef = useRef()
  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */
  const handleClose = (): void => {
    if (onClose) onClose()
  }

  const handleBack = (): void => {
    if (onBack) onBack()
  }

  /*
   * Side effects
   */
  useClickOutside(sheetRef, handleClose, closeOnBackdropClick)

  /*
   * Return
   */
  return (
    <Box
      sx={theme => ({
        display: 'flex',
        flexDirection: 'column',
        height: backDrop ? '100%' : 'auto',
        position: 'fixed',
        background:
          backDrop && slideProps.in
            ? alpha(theme.palette.primary.dark, 0.8)
            : 'transparent',
        bottom: 0,
        left: 0,
        right: 0,
        transition: theme.transitions.create(['background', 'z-index'], {
          duration: slideProps.in
            ? theme.transitions.duration.short
            : theme.transitions.duration.shortest,
          easing: slideProps.in
            ? theme.transitions.easing.easeOut
            : theme.transitions.easing.easeOut,
        }),
        justifyContent: 'flex-end',
        zIndex: slideProps.in ? theme.zIndex.modal - 1 : -1,
        '& .MuiTypography-root': {
          color: theme.palette.primary.dark + ' !important',
        },
      })}
    >
      <Slide
        {...slideProps}
        ref={sheetRef}
        id={uuid()}
        direction='up'
        easing={{
          enter: TransitionEasing.sharp,
          exit: TransitionEasing.easeOut,
        }}
        timeout={{
          enter: TransitionDuration.standard,
          exit: TransitionDuration.shortest,
        }}
      >
        <Paper
          sx={theme => ({
            borderRadius: '24px 24px 0 0',
            background: getThemeGradient(theme.palette.primary.main),
          })}
        >
          <Box
            sx={{
              display: 'flex',
              justifyContent: onBack ? 'space-between' : 'flex-end',
              m: 1,
            }}
          >
            {onBack && (
              <IconButton aria-label='back' onClick={handleBack} color='primary'>
                <ArrowBackOutlined />
              </IconButton>
            )}
            {onClose && (
              <IconButton aria-label='delete' onClick={handleClose} color='primary'>
                <CloseOutlined />
              </IconButton>
            )}
          </Box>
          <Box sx={{ px: 2, pb: 6, mt: onBack || onClose ? 0 : 4 }}>
            {title && (
              <Typography
                variant='h2'
                textAlign='center'
                sx={{ mb: subtitle ? 1 : 5, mx: 5, color: 'primary.dark' }}
              >
                {title}
              </Typography>
            )}
            {subtitle && (
              <Typography
                variant='h6'
                textAlign='center'
                sx={{ mb: 5, color: 'primary.dark' }}
              >
                {subtitle}
              </Typography>
            )}
            {children}
          </Box>
        </Paper>
      </Slide>
    </Box>
  )
}
