import {
  alpha,
  Avatar,
  ListItemButton,
  ListItemButtonProps,
  ListItemText,
  SvgIconTypeMap,
} from '@mui/material'
import { OverridableComponent } from '@mui/material/OverridableComponent'
import React, { FC } from 'react'

interface TrackActionProps extends ListItemButtonProps {
  primary?: string
  secondary?: string
  selected?: boolean
  Icon: OverridableComponent<SvgIconTypeMap<Record<string, unknown>, 'svg'>> & {
    muiName: string
  }
}

export const TrackAction: FC<TrackActionProps> = props => {
  /*
   * Component state
   */
  const { primary, secondary, selected, Icon, ...listItemButtonProps } = props

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */

  /*
   * Side effects
   */

  /*
   * Return
   */

  return (
    <ListItemButton {...listItemButtonProps} sx={{ px: 0, ...listItemButtonProps.sx }}>
      <Avatar
        variant='rounded'
        sx={theme => ({
          mr: 2,
          backgroundColor: selected ? theme.palette.primary.dark : 'transparent',
          border: `2px solid ${alpha(theme.palette.primary.dark, selected ? 1 : 0.1)}`,
        })}
      >
        <Icon
          fontSize='large'
          sx={{
            color: selected ? 'primary.main' : 'primary.dark',
            opacity: selected ? 1 : 0.4,
          }}
        />
      </Avatar>
      <ListItemText primary={primary} secondary={secondary} />
    </ListItemButton>
  )
}
