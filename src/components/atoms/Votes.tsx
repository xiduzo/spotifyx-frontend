import { ArrowUpward, ArrowDownward } from '@mui/icons-material'
import { Box, BoxProps, IconButton, Typography } from '@mui/material'
import { FC, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { useSocket } from '../../hooks/useSocket'
import { useSpotify } from '../../hooks/useSpotify'
import { Sign, Vote } from '../../redux/reducer'
import { selectRoomPin, selectTrackVotes } from '../../redux/selectors'
import { myVote } from '../../utils/myVote'
import { countVotes } from '../../utils/voteCounter'

interface VotesProps extends BoxProps {
  trackId: string
}

export const Votes: FC<VotesProps> = props => {
  /*
   * Component state
   */
  const { trackId, ...boxProps } = props

  const _roomId = useSelector(selectRoomPin)
  const _votes = useSelector(selectTrackVotes(trackId))

  /*
   * Custom & 3th party hooks
   */
  const { userId } = useSpotify()
  const { socket } = useSocket()

  const _myVote = useMemo(() => myVote(_votes, userId), [_votes, userId])

  /*
   * Methods
   */
  const vote = (sign: Sign): void => {
    if (!socket) return
    if (!userId) return

    const currentVote = myVote(_votes, userId)

    if (currentVote === sign) return

    const _vote: Vote = {
      sign,
      at: new Date(),
      by: userId,
      track: trackId,
    }

    socket.emit('vote:track', _vote, _roomId)
  }

  /*
   * Side effects
   */

  /*
   * Return
   */
  return (
    <Box {...boxProps} display='flex' flexDirection='column' alignItems='center'>
      <IconButton
        size='small'
        sx={{
          color: _myVote === Sign.Positive ? 'success.main' : 'inherit',
        }}
        onClick={() => vote(Sign.Positive)}
      >
        <ArrowUpward />
      </IconButton>
      <Typography variant='caption'>
        {countVotes(_votes, Sign.Positive) - countVotes(_votes, Sign.Negative)}
      </Typography>
      <IconButton
        size='small'
        sx={{
          color: _myVote === Sign.Negative ? 'error.main' : 'inherit',
        }}
        onClick={() => vote(Sign.Negative)}
      >
        <ArrowDownward />
      </IconButton>
    </Box>
  )
}
