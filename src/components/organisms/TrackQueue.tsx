import { ArrowDownwardOutlined, ArrowUpwardOutlined } from '@mui/icons-material'
import { Box, Divider, List, Typography } from '@mui/material'
import { BoxProps } from '@mui/system'
import { FC, useCallback, useState } from 'react'
import { useSelector } from 'react-redux'
import { selectQueue } from '../../redux/selectors'
import { BottomSheet } from '../atoms/BottomSheet'
import { Track } from '../atoms/Track'
import { TrackAction } from '../atoms/TrackAction'
import { QueuedTrack } from '../molecules/QueuedTrack'

export const TrackQueue: FC<BoxProps> = props => {
  /*
   * Component state
   */
  const { ...boxProps } = props
  const _queue = useSelector(selectQueue)
  const tracks = _queue.slice(1, _queue.length)

  const [selectedTrack, setSelectedTrack] = useState<SpotifyApi.TrackObjectFull>()

  /*
   * Custom & 3th party hooks
   */

  /*
   * Methods
   */
  const selectTrackHandler = useCallback((track?: SpotifyApi.TrackObjectFull): void => {
    setSelectedTrack(track)
  }, [])

  const closeBottomSheet = (): void => {
    setSelectedTrack(undefined)
  }

  /*
   * Side effects
   */

  /*
   * Return
   */
  // TODO: do we need this complexity of virtualized lists?
  // expect to have playlist of about 200 items, react can handle this.
  return (
    <Box flexGrow={1} {...boxProps}>
      <Typography variant='h2' sx={{ pb: 3 }}>
        Queue
      </Typography>
      <List>
        {tracks.map((track, index) => (
          <QueuedTrack
            key={track + index}
            track={tracks[index]}
            onClick={selectTrackHandler}
          />
        ))}
      </List>
      <BottomSheet
        in={!!selectedTrack}
        backDrop
        closeOnBackdropClick={true}
        onClose={closeBottomSheet}
      >
        <Box>
          <List sx={{ p: 0 }}>
            <Track track={selectedTrack} disableRipple disableTouchRipple />
          </List>
          <Divider sx={{ my: 3.5 }} />
          <List sx={{ p: 0 }}>
            <TrackAction
              Icon={ArrowUpwardOutlined}
              primary='Upvote song'
              secondary='And it will move up in the queue'
              selected
            />
            <TrackAction
              Icon={ArrowDownwardOutlined}
              primary='Downvote song'
              secondary='And it will move down in the queue'
            />
          </List>
        </Box>
      </BottomSheet>
    </Box>
  )
}
