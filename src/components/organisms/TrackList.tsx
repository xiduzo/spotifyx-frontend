import { Box, Grow, List } from '@mui/material'
import { FC, memo } from 'react'
import { TransitionGroup } from 'react-transition-group'
import { timeout } from '../../utils/animation'
import { Track } from '../atoms/Track'

interface TrackListProps {
  tracks: SpotifyApi.TrackObjectFull[]
  onTrackClick: (track: SpotifyApi.TrackObjectFull) => () => void
}

export const TrackList: FC<TrackListProps> = memo(props => (
  <TransitionGroup component={List}>
    {props.tracks.map((track, index) => (
      <Grow key={track.id} timeout={timeout(index)}>
        <Box>
          <Track onClick={props.onTrackClick(track)} track={track} selectable />
        </Box>
      </Grow>
    ))}
  </TransitionGroup>
))
