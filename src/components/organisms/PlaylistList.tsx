import { timeout } from '../../utils/animation'
import { TransitionGroup } from 'react-transition-group'
import { List, Grow, Box } from '@mui/material'
import { FC, memo } from 'react'
import { Playlist } from '../atoms/Playlist'

interface PlaylistListProps {
  playlists: SpotifyApi.PlaylistObjectSimplified[]
  onPlaylistClick: (playlist: SpotifyApi.PlaylistObjectSimplified) => () => void
}

export const PlaylistsList: FC<PlaylistListProps> = memo(props => (
  <TransitionGroup component={List}>
    {props.playlists.map((playlist, index) => (
      <Grow key={playlist.id} timeout={timeout(index)} onExited={undefined}>
        <Box onClick={props.onPlaylistClick(playlist)}>
          {/* TODO: fix forward ref on playlist */}
          <Playlist playlist={playlist} />
        </Box>
      </Grow>
    ))}
  </TransitionGroup>
))
